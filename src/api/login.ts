import httpRequest from '../utils/index';
import { ResponseData, LoginData } from './type';
export const login = (params: any) =>
  httpRequest.post<ResponseData<LoginData>>({
    url: '/login?grant_type=admin',
    data: params,
  });
