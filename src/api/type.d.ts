export interface ResponseData<T> {
  status: number;
  statusText: string;
  data: T;
}

export interface LoginData {
  access_token: string;
  authorities: any[];
  expires_in: number;
  refresh_token: string;
  shopId: number;
  token_type: string;
  userId: number;
}

export interface HomeData {
  list: any[];
}
