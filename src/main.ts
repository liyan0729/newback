import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import { createPinia } from 'pinia';
import Antd from 'ant-design-vue';
import uuid from 'vue-uuid';
import 'ant-design-vue/dist/antd.css';
import piniaPersist from 'pinia-plugin-persist'; // pinia持久化存储插件pinia-plugin-persist
const pinia = createPinia();
const app = createApp(App);
pinia.use(piniaPersist);
app.use(Antd);
app.use(uuid);
app.use(pinia);
app.use(router);
app.mount('#app');
