class LocalCache {
  setCache(key: string, val: string) {
    // 本地存储设置过期时间
    if (typeof val === 'object') {
      val = JSON.stringify(val);
    }
    window.localStorage.setItem(key, val);
  }

  getCache(key: string) {
    const val = window.localStorage.getItem(key);
    if (!val) return;
    return JSON.parse(val);
  }

  removeCache(key: string) {
    window.localStorage.removeItem(key);
  }

  clear() {
    window.localStorage.clear();
  }
}

export default new LocalCache();
