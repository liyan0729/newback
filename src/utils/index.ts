import HttpRequest from './request/httpRequest';

// console.log(HttpRequest);

const httpRequest = new HttpRequest({
  baseURL: import.meta.env.VITE_BASE_URL,
  timeout: 1000,
});

export default httpRequest;

// const login = ()=> httpRequest.post({url:'/login',data:params})
