/* eslint no-throw-literal: "error" */
import axios, { AxiosRequestConfig, AxiosInstance } from 'axios';
import { ElMessage, ElLoading } from 'element-plus';
import { LoadingInstance } from 'element-plus/lib/components/loading/src/loading';

const DEFAULT_LOADING = true;
interface httpRequestConfig extends AxiosRequestConfig {
  isShowLoading?: boolean;
}
class HttpRequest {
  intance: AxiosInstance;
  loading?: LoadingInstance;
  isShowLoading?: boolean;
  constructor(config: httpRequestConfig) {
    console.log(config, 'config');
    this.intance = axios.create(config); // 创建了axios的实例
    this.isShowLoading = config.isShowLoading || DEFAULT_LOADING;
    this.intance.interceptors.request.use(
      (config) => {
        // 在发送请求之前做些什么
        if (this.isShowLoading) {
          // 避免同一个接口重复发请求，多次显示弹框
          this.loading = ElLoading.service({
            lock: true,
            text: '正在加载中...',
            background: 'rgba(0, 0, 0, 0.7)',
          });
          console.log(config, 'config');
        }
        this.isShowLoading = false;
        return config;
      },
      function (error) {
        // 对请求错误做些什么
        return Promise.reject(error);
      }
    );
    this.intance.interceptors.response.use(
      (response) => {
        // 2xx 范围内的状态码都会触发该函数。
        // 对响应数据做点什么
        this.loading?.close();
        this.isShowLoading = DEFAULT_LOADING;
        console.log(response, 'response');
        return response;
      },
      (error) => {
        // 超出 2xx 范围的状态码都会触发该函数。
        // 对响应错误做点什么
        console.log(error, 'error');
        this.isShowLoading = DEFAULT_LOADING; // 解决了不同接口请求，显示的问题
        this.loading?.close();
        if (error.response.status === 400) {
          ElMessage.error(error.response.data || '请求错误');
        } else if (error.response.status === 401) {
          ElMessage.error(error.response.data || '没有权限');
        } else {
          ElMessage.error('请求失败错误');
        }
        return Promise.reject(error);
      }
    );
  }

  request<T>(config: httpRequestConfig): Promise<T> {
    return new Promise((resolve, reject) => {
      this.intance
        .request<T, any>(config)
        .then((res) => {
          console.log(res);
          resolve(res);
        })
        .catch((err) => {
          // 捕获异常
          reject(new Error(err));
        });
    });
  }

  get<T>(config: httpRequestConfig): Promise<T> {
    return this.request<T>({ ...config, method: 'GET' });
  }

  post<T>(config: httpRequestConfig): Promise<T> {
    return this.request<T>({ ...config, method: 'POST' });
  }
}

export default HttpRequest;
