import { createRouter, createWebHistory } from 'vue-router';
import Layout from '../layout/index.vue';
const routes = [
  {
    path: '/',
    component: Layout,
    children: [
      {
        path: '/',
        redirect: '/home/index',
      },
      {
        path: '/home/index',
        component: () => import('../views/home/index.vue'),
      },
      {
        path: '/Apply/affiche',
        component: () => import('../views/Apply/affiche.vue'),
      },
      {
        path: '/Apply/fright',
        component: () => import('../views/Apply/fright.vue'),
      },
      { path: '/Apply/hot', component: () => import('../views/Apply/hot.vue') },
      {
        path: '/Apply/point',
        component: () => import('../views/Apply/point.vue'),
      },
      {
        path: '/Apply/swipper',
        component: () => import('../views/Apply/swipper.vue'),
      },
      {
        path: '/member/menberlist',
        component: () => import('../views/member/menberlist.vue'),
      },
      {
        path: '/orders/orderlist',
        component: () => import('../views/orders/orderlist.vue'),
      },

      {
        path: '/product/classify',
        component: () => import('../views/product/classify.vue'),
      },
      {
        path: '/product/classifyitem',
        component: () => import('../views/product/classifyitem.vue'),
      },
      {
        path: '/product/comment',
        component: () => import('../views/product/comment.vue'),
      },
      {
        path: '/product/productlist',
        component: () => import('../views/product/productlist.vue'),
      },
      {
        path: '/product/rules',
        component: () => import('../views/product/rules.vue'),
      },
      {
        path: '/system/address',
        component: () => import('../views/system/address.vue'),
      },
      {
        path: '/system/adminlist',
        component: () => import('../views/system/adminlist.vue'),
      },
      {
        path: '/system/log',
        component: () => import('../views/system/log.vue'),
      },
      {
        path: '/system/menu',
        component: () => import('../views/system/menu.vue'),
      },
      {
        path: '/system/parameter',
        component: () => import('../views/system/parameter.vue'),
      },
      {
        path: '/system/timing',
        component: () => import('../views/system/timing.vue'),
      },
      {
        path: '/system/user',
        component: () => import('../views/system/user.vue'),
      },
    ],
  },
  { path: '/login', component: () => import('../views/login.vue') },
];

const router = createRouter({
  // 4. 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
  history: createWebHistory(),
  routes, // `routes: routes` 的缩写
});

export default router;
