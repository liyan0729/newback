import { defineStore } from 'pinia';
import { login } from '../../api/login';
const useUser = defineStore('user', {
  state: () => {
    return {
      token: '',
    };
  },
  actions: {
    async getToken(params: any) {
      const res = await login(params);
      this.token = res.data.token_type + res.data.access_token;
    },
  },
  persist: {
    enabled: true, // 这个配置代表存储生效，而且是整个store都存储
    strategies: [
      // { storage: sessionStorage, paths: ['firstName', 'lastName'] }, // firstName 和 lastName字段用sessionStorage存储
      { storage: localStorage, paths: ['token'] }, // accessToken字段用 localstorage存储
    ],
  },
});

export default useUser;
