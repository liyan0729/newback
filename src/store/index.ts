import useUser from './modules/user';

const useStore = () => ({
  user: useUser(),
});

export default useStore;
